import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.*;
import java.awt.*;

public class graphPanel 
	extends    JPanel 
{
	private static final int MAX_DISPLAY = 1180;
	private static final int MAX_CURRENT = 640;
	
	//class to hold information about process's
	private class graphMaintainer
	{

		public int Position;
		public int Speed;
		public int Current;
		
		public graphMaintainer(int pos, int spd, int cur)
		{
			Position = pos;
			Speed = spd;
			Current = cur;
		}
	}

	private int position = 0;
	//this array holds graphMaintainer objects, which describe each process 
	//the first dimension should line up with process numbers, and the
	//second should line up with slices being displayed
	private graphMaintainer[] slices = new graphMaintainer[MAX_DISPLAY];

	//these variables are used throughout the class for various positioning
	//features
	

	//public constructor
	public graphPanel()
	{
		super();
	}


	//removes all items from the slices array, effectivley clearing the
	//display and memory
	public void clear()
	{
		slices = new graphMaintainer[MAX_DISPLAY];

	}

	private int delay = 0;
	public void addSlice(int P, int S, int A)
	{
		slices[position] = new graphMaintainer(P,S,A);
		position = (position + 1)% MAX_DISPLAY;
		
		if(delay++==5)
		{
			paintComponent(null);
			delay = 0;
		}
	}
	

	//a custom paintComponent method, which handles drawing the graph
    	@Override
    	public void paintComponent(Graphics g)
	{
		int sliceWidth = getWidth()/MAX_DISPLAY;
		int height = getHeight();
		//if the graphics object was not passed in, use the default
		//object
		if(g == null)g = getGraphics();


		//iterate over the horizontal (draw each time slice)
		for(int i = 0; i<MAX_DISPLAY; i++)
		{
			//x is the left of this slice
			int x = i*sliceWidth;
			//starting at the left of the screen, with the oldest value
			//draw the whole canvas white (clear the slice)
			g.setColor(Color.white);
			g.fillRect(x,0,x+sliceWidth,getHeight());
			//draw a lime line at ADC=640
			g.setColor(Color.yellow);
			g.drawLine(x,height-((MAX_CURRENT*height)/1024), x+sliceWidth, height-((MAX_CURRENT*height)/1024));

			if(slices[(position+i)%MAX_DISPLAY]!=null && slices[(position+i+1)%MAX_DISPLAY]!=null)
			{
				//P first as red
				g.setColor(Color.red);
				g.drawLine(x,height-(slices[(position+i)%MAX_DISPLAY].Position*height)/14000, (x+sliceWidth), height-(slices[(position+i+1)%MAX_DISPLAY].Position*height)/14000);
				//S second as blue
				g.setColor(Color.blue);
				g.drawLine(x,height-slices[(position+i)%MAX_DISPLAY].Speed*(height/255), x+sliceWidth, height-slices[(position+i+1)%MAX_DISPLAY].Speed*(height/255));
				//A last as green
				g.setColor(Color.green);
				g.drawLine(x,height-(slices[(position+i)%MAX_DISPLAY].Current*height)/1024, x+sliceWidth, height-(slices[(position+i+1)%MAX_DISPLAY].Current*height)/1024);
			}
		}
    	}
}

#!/usr/bin/python -u

from serial  import *
from sys     import exit
from sys     import argv
from sys     import stdout


#serial init
ser = Serial(argv[1], int(argv[2]),rtscts=True,dsrdtr=True)

#main loop
while True:
	line = ser.readline().strip()
	
	print(line)

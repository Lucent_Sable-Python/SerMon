import java.io.*;
import java.util.*;


public class Display 
{
	public static void main(String[] args)
		throws IOException
	{
		int P = 0;
		int A = 0;
		int S = 0;
		//create and show the UI
		//SwingUtilities.invokeLater(new Runnable(){public void run(){gui.createUI();}});
		gui.Invoke();

		//from here, read from STDIN and update the UI
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		while(true)
		{
			String in = reader.readLine();
			if(in == null)continue;
			
			//System.out.println("Recieved Input:" + in);
			logger.log(in+'\n');
			if(gui.main_text != null)gui.main_text.append(in+'\n');
			if(gui.sig_text != null && in.contains("F"))
			{
				gui.sig_text.append(in+'\n');
			}

			try
			{
				if(gui.mem_text != null && (in.startsWith("P") || in.startsWith("S") || in.startsWith("A")))
				{
					gui.mem_text.append(in+'\n');

					String[] split = in.split(":");

					//data should arrive in PAS sets
					//split data into PAS
					if(in.startsWith("P"))
					{
						P = Integer.parseInt(split[1]);
					}
					if(in.startsWith("A"))
					{
						A = Integer.parseInt(split[1]);
					}
					if(in.startsWith("S"))
					{
						S = Integer.parseInt(split[1]);
						//S is always the last to be recieved, therefore update the graph
						gui.hist_graph.addSlice(P,S,A);
					}
				}
			}	
			catch(NumberFormatException e)
			{
				gui.clear();
			}	
			catch(ArrayIndexOutOfBoundsException e)
			{
				gui.clear();
			}

		}
	}
}

import java.io.*;

class logger
{
	static FileWriter logFile = null;

	public static void log(String s)
		throws IOException
	{
		if(logFile==null)begin();
		logFile.write(s);
	}

	public static void begin()
		throws IOException
	{
		if(logFile!=null)logFile.close();
		File f = new File("Minix"+System.currentTimeMillis());
		logFile = new FileWriter(f);				
	}

	public static void end()
	{
		if(logFile!=null)
		{
			try
			{
				logFile.close();
				logFile = null;
			}
			catch(IOException e)
			{//do nothing
			}
		}
	}
}



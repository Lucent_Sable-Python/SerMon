//GUI
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

class gui
{
	//externally accessable objects
	public static JTextArea     main_text = null;
	public static JTextArea     sig_text = null;
	public static JTextArea     mem_text = null;
	       static JFrame        root = null;
	public static graphPanel    hist_graph = null;

	//on calling envoke, start the swing rendering/event thread
	public static void Invoke()
	{
 		SwingUtilities.invokeLater(new Runnable(){public void run(){gui.createUI();}});
	}
	
	//the event to fire when the window is being closed
	static WindowAdapter closeAdapter = new WindowAdapter()
	{
		@Override
		public void windowClosing(WindowEvent e)
		{
			//close the logger
			logger.end();
			//exit the program
			if(root !=null)root.dispose();
			System.exit(0);
		}
	};

	public static void clear()
	{
		main_text.setText(null);
		sig_text.setText(null);
		mem_text.setText(null);
		hist_graph.clear();
	}

	//this process creates the user interface. It is a fairly long method,
	//which hasa to individually initialise each display element, and lay
	//them out in a sensible manor
	public static void createUI()
	{
		//these borders and dimensions are the default for
		//text-information panels
		Border paneFrame = BorderFactory.createEmptyBorder(10,10,10,10);
		Dimension TextSize = new Dimension(400,300);
		Dimension HalfTextSize = new Dimension(400,150);
		//create the root frame and set some properties
		JFrame root = new JFrame("Elevator");
		root.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		root.addWindowListener(closeAdapter);
		root.setResizable(false);

		
		//put the serial inputs into a panel
		JPanel	text_panel = new JPanel();
		text_panel.setLayout(new FlowLayout());
	
		//main textbox, which gets all input
		main_text = new JTextArea();
		JScrollPane main_scroll = new JScrollPane(main_text);
		main_scroll.setBorder(paneFrame);
		main_scroll.setPreferredSize(TextSize);
		new SmartScroller(main_scroll);


		//have a panel to hold both SIG and MEM output
		JPanel SIG_MEM_panel = new JPanel();
		//first text-box gets all SIG events
		sig_text = new JTextArea();
		JScrollPane sig_scroll = new JScrollPane(sig_text);
		sig_scroll.setBorder(paneFrame);
		sig_scroll.setPreferredSize(HalfTextSize);
		new SmartScroller(sig_scroll);

		//all MEM events
		mem_text = new JTextArea();
		JScrollPane mem_scroll = new JScrollPane(mem_text);
		mem_scroll.setBorder(paneFrame);
		mem_scroll.setPreferredSize(HalfTextSize);
		new SmartScroller(mem_scroll); 

		//the graph panel
		JPanel hist_panel = new JPanel();
		hist_graph = new graphPanel();
		JScrollPane hist_scroll = new JScrollPane(hist_graph);
		hist_scroll.setBorder(paneFrame);
		hist_scroll.setPreferredSize(new Dimension(1200,400));
		new SmartScroller(hist_scroll);


		//add components to the layout managing panels
		text_panel.add(main_scroll);
		text_panel.add(SIG_MEM_panel);
		text_panel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		SIG_MEM_panel.add(sig_scroll);
		SIG_MEM_panel.add(mem_scroll);
		SIG_MEM_panel.setLayout(new BoxLayout(SIG_MEM_panel,BoxLayout.Y_AXIS));

		hist_panel.add(hist_scroll);

		//add the layout managing panels to the root frame
		root.getContentPane().add(text_panel, BorderLayout.LINE_START);
		root.getContentPane().add(hist_panel, BorderLayout.PAGE_END);
		root.pack();
		//display the GUI
		root.setVisible(true);
	} 	
}

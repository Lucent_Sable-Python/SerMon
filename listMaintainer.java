import java.util.*;

class listMaintainer
{
	//a private class, which implements a list item for the process list
	private static class process
	{
		//the comparator for sorting elements of the list
		private static	class comparator implements Comparator<process>
		{
			public int compare(process p1, process p2)
			{
				return p1.getPN() - p2.getPN();
			}
		}

		//the process name and number
		private int PN;
		public String name;

		public process(int pn, String n)
		{
			PN = pn;
			name = n;
		} 

		public int getPN()
		{
			return PN;
		}
	}


	//the list which holds currently running process's
	public static ArrayList<process> currentProcs = new ArrayList<process>();

	//add a process to the list
	public static void AddProcess(int pn, String name)
	{
		currentProcs.add(new process(pn,name));
		
		//now update the process list
		update_disp();
	}	

	//export list data to the listbox, which will display them nicely
	static void update_disp()
	{
		if(gui.proc_list!=null) gui.proc_list.setListData(getStrings());
	}

	//remove element from the list
	public static void RemoveProcess(int pn)
	{
		Iterator<process> pIterator = currentProcs.iterator();
		while(pIterator.hasNext())
		{
			//remove elements which have the desired PID
			//(Should only be one)
			if(pIterator.next().getPN() == pn)pIterator.remove();
		}
		
		update_disp();
	}

	//rename a process that is currently in the list (i.e. for an exec)
	public static void RenameProcess(int pn, String name)
	{
		//iterate through the list, looking for the process number
		Iterator<process> pIterator = currentProcs.iterator();

		while(pIterator.hasNext())
		{
			process next = pIterator.next();
			//if the process is found, remane it
			if(next.getPN() == pn) next.name = name;
		}

		update_disp();
	}

	//clears out the memory
	public static void clear()
	{
		currentProcs.clear();
		update_disp();
	}

	//sorts the list
	static void sort()
	{
		Collections.sort(currentProcs, new process.comparator());
	}
	
	//converts the list to a string array
	public static String[] getStrings()
	{
		//get the size of the current list
		int c = currentProcs.size();
		int i = 0;
		//sort the list by process number
		sort();
		
		String[] out = new String[c];
		
		//iterate over the entire list
		Iterator<process> pIterator = currentProcs.iterator();

		while(pIterator.hasNext())
		{
			//add each process in the list to the string array
			process p = pIterator.next();
			out[i++] = p.getPN() + "  "  + p.name;
		}

		return out;
	}
}


